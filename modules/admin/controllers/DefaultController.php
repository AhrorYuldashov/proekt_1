<?php
namespace app\modules\admin\controllers;

use app\modules\admin\components\AController;

class DefaultController extends AController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}