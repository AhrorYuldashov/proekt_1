<?php
namespace app\modules\admin\components;

use yii\web\Controller;
use Yii;

class AController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest and !Yii::$app->user->identity) {
            return $this->goHome();
        }
        return parent::beforeAction($action);
    }
}