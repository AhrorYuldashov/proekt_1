<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m210628_044455_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'full_name'=>$this->string(255),
            'username'=>$this->string(50)->unique(),
            'email'=>$this->string(255)->unique(),
            'password'=>$this->string(255),
            'birthday'=>$this->dateTime(),
            'gender'=>$this->string()->null(),
            'photo'=>$this->string()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
