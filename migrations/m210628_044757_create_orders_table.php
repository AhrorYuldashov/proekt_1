<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders}}`.
 */
class m210628_044757_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orders}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'order_date' => $this->dateTime(),
            'status' => $this->integer()->defaultValue(0)->comment('0 - yangi, 1 - yo\'lda, 2 - yetkazib berildi, 4- qaytarildi'),

        ]);

        $this->createIndex(
            'idx-orders-product_id',
            'orders',
            'product_id'
        );

        $this->addForeignKey(
            'fk-orders-product_id',
            'orders',
            'product_id',
            'products',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-orders-user_id',
            'orders',
            'user_id'
        );

        $this->addForeignKey(
            'fk-orders-users_id',
            'orders',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%orders}}');
    }
}
